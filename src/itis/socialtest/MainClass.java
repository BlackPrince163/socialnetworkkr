package itis.socialtest;


import itis.socialtest.entities.Author;
import itis.socialtest.entities.Post;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;


/*
 * В папке resources находятся два .csv файла.
 * Один содержит данные о постах в соцсети в следующем формате: Id автора, число лайков, дата, текст
 * Второй содержит данные о пользователях  - id, никнейм и дату рождения
 *
 * Напишите код, который превратит содержимое файлов в обьекты в package "entities"
 * и осуществите над ними следующие опреации:
 *
 * 1. Выведите в консоль все посты в читабельном виде, с информацией об авторе.
 * 2. Выведите все посты за сегодняшнюю дату
 * 3. Выведите все посты автора с ником "varlamov"
 * 4. Проверьте, содержит ли текст хотя бы одного поста слово "Россия"
 * 5. Выведите никнейм самого популярного автора (определять по сумме лайков на всех постах)
 *
 * Для выполнения заданий 2-5 используйте методы класса AnalyticsServiceImpl (которые вам нужно реализовать).
 *
 * Требования к реализации: все методы в AnalyticsService должны быть реализованы с использованием StreamApi.
 * Использование обычных циклов и дополнительных переменных приведет к снижению баллов, но допустимо.
 * Парсинг файлов и реализация методов оцениваются ОТДЕЛЬНО
 *
 *
 * */

public class MainClass {

    private List<Post> allPosts;

    private AnalyticsService analyticsService = new AnalyticsServiceImpl();

    public static void main(String[] args) {
        new MainClass().run("C:\\Users\\niazh\\IdeaProjects\\socialnetworktest2\\src\\itis\\socialtest\\resources\\PostDatabase.csv", "C:\\Users\\niazh\\IdeaProjects\\socialnetworktest2\\src\\itis\\socialtest\\resources\\Authors");
    }

    private void run(String postsSourcePath, String authorsSourcePath) {
        String lineAuthor = "";
        String linePost = "";
        try {
            BufferedReader brPost = new BufferedReader(new FileReader(postsSourcePath));
            BufferedReader brAuthor = new BufferedReader(new FileReader(authorsSourcePath));
            while ((lineAuthor = brAuthor.readLine()) != null) {
                String[] values = lineAuthor.split(",");
                Author author = new Author(Long.parseLong(values[0]), values[1], values[2]);
                System.out.println(author);
            }
            while ((linePost = brPost.readLine()) != null) {
                String[] values = linePost.split(",");
               // Author author = new Author(values[0], )
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
