package itis.socialtest;

import itis.socialtest.entities.Post;

import java.util.List;
import java.util.OptionalLong;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

public class AnalyticsServiceImpl implements AnalyticsService {
    @Override
    public List<Post> findPostsByDate(List<Post> posts, String date) {
    //2. Выведите все посты за сегодняшнюю дату
        return posts.stream()
                .filter(post -> post.getDate().equals(date))
                .collect(Collectors.toList());
    }

    @Override
    public String findMostPopularAuthorNickname(List<Post> posts) {
        OptionalLong maxLike = posts.stream()
                .flatMapToLong(post -> LongStream.of(post.getLikesCount()))
                .max();
            return posts.stream()
                    .filter(post -> post.getLikesCount() == maxLike.getAsLong())
                    .collect(Collectors.toList()).get(0).getAuthor().getNickname();
    }

    @Override
    public Boolean checkPostsThatContainsSearchString(List<Post> posts, String searchString) {
        //4. Проверьте, содержит ли текст хотя бы одного поста слово "Россия"
        //проверить сообщения, содержащие SearchString
        return posts.stream()
                .anyMatch(post -> post.getContent().equals(searchString));
    }

    @Override
    public List<Post> findAllPostsByAuthorNickname(List<Post> posts, String nick){
        //найти все сообщения по нику автора
        return posts.stream()
                .filter(post -> post.getAuthor().getNickname().equals(nick))
                .collect(Collectors.toList());
    }
}
